package com.tw;

public class StringCalculator {
    public int add(String string) {
        int sum = 0;
        if (string.isBlank()) {
            return 0;
        }

        String delimiter = ",|\n|;";
        String[] numbers = string.split(delimiter);
        for (String number : numbers) {
            try {
                sum += Integer.parseInt(number);
            } catch (NumberFormatException e) {
                sum += 0;
            }
        }
        return sum;
    }
}
